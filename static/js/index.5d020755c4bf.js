function checkFormInput() {
	console.log('Checking Form Input'); 
	if ($('#username').val() && $("#password").val() && $("#email").val() && emailOK) {
		$('#SubmitButton').removeAttr("disabled");
	} else {
		$('#SubmitButton').attr("disabled", "disabled");
	}
	console.log(emailOK);
}


var emailOK = false;
$(document).ready(function() {
	checkFormInput();

	$(document).on('keyup', '#username, #email, #password', function() {
		checkFormInput();
	});

	//DEBUG
	//console.log(emailField.val());
	//console.log(urnameField.val());
	//console.log(pwdField.length);

	$('#email').on('change', function() {
		url = '/checkEmail/' + $('#email').val();
		console.log(url);
		$.ajax({
			url: url,
			method: 'GET',
			data: $('#email').val(),
			success: function(response) {
				console.log(response);
				if (response.isOk) {
					console.log("Email is OK");
				} else {
					console.log("Email is not OK");
				}
			}
		}).then(function(response) {
			if (response.isOk) {
				emailOK = true;
			}
			console.log("EmailOK: " + emailOK);
		});
	});

	$("#subscriberForm").on('submit', function(event) {
		event.preventDefault();
		$.ajax({
			type: "POST",
			url: "/subscribe",
			data: {
				username: $("#username").val(),
				email: $("#email").val(),
				password: $("#password").val(),
				csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
			},
			success: function(response) {
				alert($("#username").val() + " has been registered as an user!");
				$("#username").val("");
				$("#email").val("");
				$("#password").val("");
				console.log("OK");
			}
		})
	});
});

// $(document).ready(submitForm());
