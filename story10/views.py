from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.core.serializers import serialize
from .forms import SubscriberForm
from .models import Subscriber

# Create your views here.
response = {}
def index(request):
	response['form'] = SubscriberForm()
	return render(request, 'index.html', response)

def subscribe(request):
	if request.method == "POST":
		print('Masuk POST')
		form = SubscriberForm(request.POST)
		if form.is_valid():
			print("OK")
			newuser = form.save()
		
	return HttpResponse("OK")

def checkEmailAvailability(request, email):
	email_is_empty = Subscriber.objects.filter(email=email).count() == 0
	json_data = {'isOk': email_is_empty}
	return JsonResponse(json_data)

def getSubscribersData(request):
	json_data = serialize('json', Subscriber.objects.all())

	print(json_data)
	return JsonResponse(json_data, safe=False)

def unsubscribe(request):
	print(request.POST)
	email = request.POST['email']
	password = request.POST['password']
	target = Subscriber.objects.filter(email=email, password=password)
	if target:
		target.delete()
		return HttpResponse("Deleted")
	return HttpResponse("Finished")