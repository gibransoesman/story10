from django.urls import path
from . import views

urlpatterns = [
	path('', views.index, name='index'),
	path('subscribe', views.subscribe, name='subscribe'),
	path('checkEmail/<str:email>', views.checkEmailAvailability),
	path('getSubscribersData', views.getSubscribersData),
	path('unsubscribe/', views.unsubscribe),
]