from django.test import TestCase, Client
from django.urls import resolve
from .views import index, checkEmailAvailability, subscribe
from .models import Subscriber
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from time import sleep

# Create your tests here.
class Story10FunctionalTest(TestCase):
	def setUp(self):
		firefox_options = Options()
		firefox_options.add_argument("--headless")
		self.browser = webdriver.Firefox(firefox_options=firefox_options)

	def tearDown(self):
		self.browser.quit()

	def test_register(self):
		self.browser.get("http://localhost:8000")
		
		userNameField = self.browser.find_element_by_id('username')
		emailField = self.browser.find_element_by_id('email')
		pwdField = self.browser.find_element_by_id('password')
		
		userNameField.send_keys("abcdef")
		emailField.send_keys("abc@def.gh")
		pwdField.send_keys("fedcba")

		self.assertEqual(userNameField.get_attribute('value'), 'abcdef')
		self.assertEqual(emailField.get_attribute('value'), 'abc@def.gh')
		self.assertEqual(pwdField.get_attribute('value'), 'fedcba')

		submitButton = self.browser.find_element_by_id('SubmitButton')
		submitButton.click()
		self.tearDown()

class Story10Test(TestCase):
	def test_story10_url_exists(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_story10_correct_function(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_story10_correct_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')

	def test_checkEmail_url_exists(self):
		test = "abcde@a.bc"
		response = Client().get('/checkEmail/' + test)
		self.assertEqual(response.status_code, 200)

	def test_checkEmail_correct_function(self):
		test = "abcd@a.bc"
		found = resolve('/checkEmail/' + test)
		self.assertEqual(found.func, checkEmailAvailability)

	def test_add_user(self):
		request = {
			'username': 'abcde',
			'email': 'abcde@a.bc',
			'password': 'edcba',
		}

		response = Client().post('/subscribe', request)
		html_resp = response.content.decode('utf8')
		self.assertIn("OK", html_resp)
		self.assertEqual(response.status_code, 200)

	def test_subscribe_correct_function(self):
		found = resolve('/subscribe')
		self.assertEqual(found.func, subscribe)

	def test_existing_email(self):
		Subscriber.objects.create(username="test", email="test@test.st", password="tsettest")
		response = Client().get('/checkEmail/' + 'test@test.st')
		html_resp = response.content.decode('utf8')
		self.assertJSONEqual(html_resp, {'isOk': False})