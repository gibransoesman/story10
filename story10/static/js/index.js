function checkFormInput() {
	console.log('Checking Form Input'); 
	if ($('#username').val() && $("#password").val() && $("#email").val() && emailOK) {
		$('#SubmitButton').removeAttr("disabled");
	} else {
		$('#SubmitButton').attr("disabled", "disabled");
	}
	console.log(emailOK);
}


var emailOK = false;
$(document).ready(function() {
	checkFormInput();

	$(document).on('keyup', '#username, #email, #password', function() {
		checkFormInput();
	});

	$('#email').on('change', function() {
		url = '/checkEmail/' + $('#email').val();
		console.log(url);
		$.ajax({
			url: url,
			method: 'GET',
			data: $('#email').val(),
			success: function(response) {
				console.log(response);
				if (response.isOk) {
					console.log("Email is OK");
				} else {
					console.log("Email is not OK");
				}
			}
		}).then(function(response) {
			if (response.isOk) {
				emailOK = true;
			}
			console.log("EmailOK: " + emailOK);
		});
	});

	$("#subscriberForm").on('submit', function(event) {
		event.preventDefault();
		$.ajax({
			type: "POST",
			url: "/subscribe",
			data: {
				username: $("#username").val(),
				email: $("#email").val(),
				password: $("#password").val(),
				csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
			},
			success: function(response) {
				alert($("#username").val() + " has been registered as an user!");
				$("#username").val("");
				$("#email").val("");
				$("#password").val("");
				console.log("OK");
			}
		})
	});
});

$(document).ready(function() {
	console.log("Grabbing subscriber data...");
	$.ajax({
		url: '/getSubscribersData',
		dataTyoe: 'json',
		success: function(response) {
			console.log("Successfully grabbed!");
			var entry = "<tbody id='innerbody'>";
			entry += cleanData(response);
			entry += "</tbody>";
			$("#subscribersData").append(entry);
			console.log("Subscribers data appended!");
		}
	})
})

var focused_id;
function set_focus(id) {
	focused_id = id;
}

function cleanData(input) {
	var output = "";
	data = JSON.parse(input);
	console.log(data.length);
	for (var i = 0; i < data.length; i++) {
		var username = data[i].fields['username'];
		var email = data[i].fields['email'];
		output +=
			"<td>" + username + "</td>" +
			"<td id='email" + i + "'>" + email + "</td>" +
			"<td><button class='btn btn-danger' id='unsubscribe" + i + "' data-toggle='modal' data-target='#confirmUnsubscribe' onclick = 'set_focus(id)'>Unsubscribe</td>" +
			"</tr>";
	}
	console.log("Cleaned");
	return output;
}

function unsubscribe() {
	console.log(focused_id + " is focused");
	var idNum = parseInt(focused_id.match(/\d+$/));
	console.log(idNum);
	var email = document.getElementById("email"+idNum).innerHTML;
	console.log(email);
	$.ajax({
		url: '/unsubscribe/',
		method: 'POST',
		data: {
		csrfmiddlewaretoken: $('[name=csrfmiddlewaretoken]').val(),
        email: email,
        password: $("#confirmPassword").val(),
		},
	});
}
